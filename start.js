
process.stdin.resume();
process.stdin.setEncoding('utf8');
var util = require('util');
var opn = require('opn');
var youTubeUsers = require('./resources/youtubers');
var exec = require('child_process').exec;
var options = {
  9: {
    func: function(){
      exec('subl .');
    },
    description: "Open:Qwe",
  },
  3 : {
    func: function() {
      opn(
        'http://google.com',
        {
          app: 
            [
            'chrome',
            '--incognito'
            ]
        }
      );
    },
    description: 'Web:Chrome',
  },
  4: {
    func: function() {
      opn(
        'http://facebook.com',
        {
          app: 
            [
            'chrome',
            '--incognito'
            ]
        }
      );
    },
    description: 'Web:Facebook',
  },
  2: {
   func: function() {
     openAllYoutubeUserVideos();
   },
   description: 'Web:AllYoutubersVids',
  },
  1: {
    func: function() {
      opn(
        'http://youtube.com',
        {
          app: 
            [
            'firefox',
            '-private-window'
            ]
        }
        );
    },
    description: 'Web:Youtube'
  },
  0: {
    func: function() {
      process.exit();
    },
    description: 'Exit'
  }
};

function printWelcomeMessage(){
  var message = ' Welcome! Choose your destiny: ';
  var stars = function(msg){
    var starString = '';
    for (var i = 0; i < msg.length; i++) {
      starString = starString + '*';
    }
    return starString;
  }
  console.log(stars(message));
  console.log('\n');
  console.log(message);
  console.log('\n');
  console.log(stars(message));
}

function printUserSelectionOptions(){
  for(var key in options){
    if(options[key]){
      console.log('[' + key + ']: ' + options[key].description + '\n');
    }
  }
}

function getUserInput(){
  process.stdin.on('data', function (text) {
  text = text.replace('\r\n', '');
    if(options[text]){
      options[text].func();
      printInitMessages();
    } else {
      console.log('Could not find option ' + text);
      printInitMessages();
    }
  });
}

function openInFirefox(url) {
   opn(
          url,
          {
            app: 
              [
              'firefox',
              '-private-window'
              ]
          }
    );
}

function openAllYoutubeUserVideos(){
  var preUrl = 'https://www.youtube.com/user/';
  var postUrl = '/videos';
  for (var i = 0; i < youTubeUsers.length; i++) {
    openInFirefox(preUrl + youTubeUsers[i] + postUrl);
  }
}

function printInitMessages(){
  printWelcomeMessage();
  printUserSelectionOptions();
}

printInitMessages();
getUserInput();


